import React, { useRef, useEffect } from 'react';
import { useLocation, Switch, Route } from 'react-router-dom';
import AppRoute from './utils/AppRoute';
import ScrollReveal from './utils/ScrollReveal';
import ReactGA from 'react-ga';


import LayoutDefault from './layouts/LayoutDefault';


import Home from './views/Home';
import About from './views/About';
import Header from './components/layout/Header';
import Footer from './components/layout/Footer';
import Flugx from './views/Flugx';
import Contact from './views/Contact';


ReactGA.initialize(process.env.REACT_APP_GA_CODE);

const trackPage = page => {
  ReactGA.set({ page });
  ReactGA.pageview(page);
};

const App = () => {

  const childRef = useRef();
  let location = useLocation();

  useEffect(() => {
    const page = location.pathname;
    document.body.classList.add('is-loaded')
    childRef.current.init();
    trackPage(page);
    
  }, [location]);

  return (
    <div>
      <Header/>
    <ScrollReveal
      ref={childRef}
      children={() => (
        
        <Switch>
          <Route exact path="/" component={Home} layout={LayoutDefault} />
          <Route path="/about" component={About} />
          <Route path="/flugx" component={Flugx} />
          <Route path="/contact" component={Contact} />
        </Switch>
        
      )} />
        <Footer/>
      </div> );
}

export default App;