import React from 'react';

const LetsChat = ({
    className,
    topOuterDivider,
    bottomOuterDivider,
    topDivider,
    bottomDivider,
    hasBgColor,
    invertColor,
    invertMobile,
    invertDesktop,
    alignTop,
    imageFill,
    ...props
  }) => {
  return (
    <section data-aos="fade-up" className="bg--dark cover height-60 aos-init aos-animate reveal-from-bottom" data-reveal-delay="100">
                <div className="container pos-vertical-center">
                    <div className="row">
                        <div className="col-md-9 col-lg-7">
                            <h1 classNames="reveal-from-bottom" data-reveal-delay="200">
                               Let's chat.
                            </h1>
							<p className="lead reveal-from-bottom" data-reveal-delay="200"> Let's see what we can do for you</p>
                            <p>
                                contact@itemtech.me <br/>
                                +212 (6) 49 89 68 12 <br/>
                                N˚ 299/H porte B Centre Commercial Cite Al Houda Agadir
                            </p>

                        </div>
                    </div>
                </div>
            </section>

  );

}

export default LetsChat;