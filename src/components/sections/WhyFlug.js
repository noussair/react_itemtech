import React from 'react';

const WhyFlug = ({
    className,
    topOuterDivider,
    bottomOuterDivider,
    topDivider,
    bottomDivider,
    hasBgColor,
    invertColor,
    invertMobile,
    invertDesktop,
    alignTop,
    imageFill,
    ...props
  }) => {
  return (
    <section data-aos="fade-up" className="text-center aos-init reveal-from-bottom" data-reveal-delay="100">
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-10 col-lg-8">
                    <h1 className="reveal-from-bottom" data-reveal-delay="200">Reality–virtuality continuum !</h1>
                    <p className="lead reveal-from-bottom" data-reveal-delay="250">
                    To meet these needs, the FLUG has developed features such as security footage analysis that engages also passengers in the management process, news feed, notification systems, and real-time Big Data analysis; all for the sole purpose of optimizing the management and dissemination of airport experience.
                    </p>
                    <p>
                        <img className="reveal-from-bottom" data-reveal-delay="400" src="https://itemtech.me/parametres/flug_x_main.png"/>
                    </p>
                </div>
            </div>
        </div>
    </section>

  );

}

export default WhyFlug;