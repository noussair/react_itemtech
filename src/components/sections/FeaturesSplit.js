import React from 'react';
import classNames from 'classnames';
import { SectionSplitProps } from '../../utils/SectionProps';
import SectionHeader from './partials/SectionHeader';
import Image from '../elements/Image';

const propTypes = {
  ...SectionSplitProps.types
}

const defaultProps = {
  ...SectionSplitProps.defaults
}

const FeaturesSplit = ({
  className,
  topOuterDivider,
  bottomOuterDivider,
  topDivider,
  bottomDivider,
  hasBgColor,
  invertColor,
  invertMobile,
  invertDesktop,
  alignTop,
  imageFill,
  ...props
}) => {

  const outerClasses = classNames(
    'features-split section',
    topOuterDivider && 'has-top-divider',
    bottomOuterDivider && 'has-bottom-divider',
    hasBgColor && 'has-bg-color',
    invertColor && 'invert-color',
    className
  );

  const innerClasses = classNames(
    'features-split-inner section-inner'
  );

  const splitClasses = classNames(
    'split-wrap',
    invertMobile && 'invert-mobile',
    invertDesktop && 'invert-desktop',
    alignTop && 'align-top'
  );

  const sectionHeader = {
    title: 'A smarter approach to credit lending',
    paragraph: 'Streamline your process while reducing risk'
  };

  return (
    <section
      {...props}
      className={outerClasses}
    >
      <div className="container">
        <div className={innerClasses}>
          <SectionHeader data={sectionHeader} className="center-content" />
          <div className={splitClasses}>

            <div className="split-item">
              <div className="split-item-content center-content-mobile reveal-from-left" data-reveal-container=".split-item">
                <div className="text-xxs text-color-primary fw-600 tt-u mb-8">
                  JOIN UN IN LAS VEGAS
                  </div>
                <h3 className="mt-0 mb-12">
                  Winner  CES VEGAS 2020
                  </h3>
                <p className="m-0">
                We're honored to be recognized among thousands of nominations to demonstrate the "Made in Morocco" technology brand at the largest tech event in the world. Join us at our exhibition space to share experiences and try our live demo.                  </p>
              </div>
              <div className={
                classNames(
                  'split-item-image center-content-mobile reveal-from-bottom',
                  imageFill && 'split-item-image-fill'
                )}
                data-reveal-container=".split-item">
                <Image
                  src="https://media.ouest-france.fr/v1/pictures/MjAxOTAxZGM2ODZmODU5OTc3ZWJiMDc4ZTM2NjA3NDYyYTc1NzE?width=1260&height=712&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=2579a32c079e9093055c813306e74bfae3ea1f9a494216fd4ee10e35c20fde21"
                  alt="Features split 01"
                  width={528}
                  height={396} />
              </div>
            </div>

            <div className="split-item">
              <div className="split-item-content center-content-mobile reveal-from-right" data-reveal-container=".split-item">
                <div className="text-xxs text-color-primary fw-600 tt-u mb-8">
                enabling AI for better airports facility management
                  </div>
                <h3 className="mt-0 mb-12">
                Flug x distributed artificial intelligence system
                  </h3>
                <p className="m-0">
                FLUG x is an end to end system that develops passenger flow analysis and control systems designed to inform passengers and shorten border crossing times and enhance security efficiency using a distributed artificial intelligence that uses existing camera infrastructure, sensors and multichannel system to provide real time data for a better airport experience.
                  </p>
              </div>
              <div className={
                classNames(
                  'split-item-image center-content-mobile reveal-from-bottom',
                  imageFill && 'split-item-image-fill'
                )}
                data-reveal-container=".split-item">
                <Image
                  src="http://itemtech.me/parametres/flug_main.png"
                  alt="Features split 02"
                  width={528}
                  height={396} />
              </div>
            </div>

            <div className="split-item">
              <div className="split-item-content center-content-mobile reveal-from-left" data-reveal-container=".split-item">
                <div className="text-xxs text-color-primary fw-600 tt-u mb-8">
                  Data matters
                  </div>
                <h3 className="mt-0 mb-12">
                Make an impact with your data
                  </h3>
                <p className="m-0">
                Linking hundreds of official and third-party data sources, Item is building the real-world intelligence on companies, people, and places that matter most to your business.
                  </p>
              </div>
              <div className={
                classNames(
                  'split-item-image center-content-mobile reveal-from-bottom',
                  imageFill && 'split-item-image-fill'
                )}
                data-reveal-container=".split-item">
                <Image
                  src="http://itemtech.me/parametres/data-driven.png"
                  alt="Features split 03"
                  width={528}
                  height={396} />
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
  );
}

FeaturesSplit.propTypes = propTypes;
FeaturesSplit.defaultProps = defaultProps;

export default FeaturesSplit;