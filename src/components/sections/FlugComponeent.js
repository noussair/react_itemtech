import React from 'react';
import classNames from 'classnames';

const FlugComponeent = ({
    className,
    topOuterDivider,
    bottomOuterDivider,
    topDivider,
    bottomDivider,
    hasBgColor,
    invertColor,
    invertMobile,
    invertDesktop,
    alignTop,
    imageFill,
    ...props
  }) => {
  return (
    <section data-aos="fade-up" className="text-center height-80 imagebg aos-init aos-animate reveal-from-bottom" data-reveal-delay="100" data-overlay="4">
        
    <div className="background-image-holder flugx_bg">
    </div>
    <div className="container pos-vertical-center">
        <div className="row">
            <div className="col-md-12">
                <h1 classNames="reveal-from-bottom" data-reveal-delay="200">Flug X distributed AI system</h1>
                <p className="lead reveal-from-bottom" data-reveal-delay="300">
                    enabling  artificial Intelligence for better   airports facility management.
                </p>
                <a target="_blank" className="btn btn--primary type--uppercase" href="https://itemtech.me/parametres/FlugX_brochure.pdf">
                    <span className="btn__text reveal-from-bottom" data-reveal-delay="400">
                        Download our brochure
                    </span>
                </a>
                <span className="block type--fine-print">or
                    <a className="nl_wave_routing reveal-from-bottom" data-reveal-delay="450" href="contact"> request a demo</a>
                </span>
            </div>
        </div>
    </div>
</section>

  );

}

export default FlugComponeent;