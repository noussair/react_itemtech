import React, { useState } from 'react';
import classNames from 'classnames';
import { SectionProps } from '../../utils/SectionProps';
import ButtonGroup from '../elements/ButtonGroup';
import Button from '../elements/Button';


const propTypes = {
  ...SectionProps.types
}

const defaultProps = {
  ...SectionProps.defaults
}

const AboutHeader = ({
  className,
  topOuterDivider,
  bottomOuterDivider,
  topDivider,
  bottomDivider,
  hasBgColor,
  invertColor,
  ...props
}) => {

 

  const outerClasses = classNames(
    'hero section center-content',
    topOuterDivider && 'has-top-divider',
    bottomOuterDivider && 'has-bottom-divider',
    hasBgColor && 'has-bg-color',
    invertColor && 'invert-color',
    className
  );

  const innerClasses = classNames(
    'hero-inner section-inner',
    topDivider && 'has-top-divider',
    bottomDivider && 'has-bottom-divider'
  );

  return (
    <div>
    <section data-aos="fade-up" className="bg--dark text-left aos-init aos-animate">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-7 col-md-9 col-12 reveal-from-bottom" data-reveal-delay="100">
                            <div className="mt--2 reveal-from-bottom" data-reveal-delay="200">
                                <h1>
                                    Our intelligence powers 
                                     and transforms industries.
                                </h1>
                                <p className="lead reveal-from-bottom" data-reveal-delay="300">
                                    Item partners with clients across a range of industries to deploy trusted intelligence into both automated and human-dependent decision-making. Through APIs and software that deliver new insights about companies, people, and places, our clients are empowered to increase revenue and drive cost-savings.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section data-aos="fade-up" className="space--md bg--secondary aos-init aos-animate">
                <div className="container">
                    <div className="row">
                        <div className="col-md-10 col-lg-9">
                            <span className="h3">
                               Item engineering is an award-winning digital company based in Agadir, MA. We believe that people empowered by data can make critical decisions, solve complex problems, and collaborate effectively. 
                            </span>
                        </div>
                    </div>
                </div>
            </section>
            <section className="switchable feature-large bg--dark">
                <div className="container">
                    <div className="row justify-content-around">
                        <div className="col-md-6">
                            <img alt="Image" className="border--round" src="https://carto.com/blog/img/posts/2016/2016-02-24-what-is-location-intelligence-and-its-benefits/LI-deepinsights.png"/>
                        </div>
                        <div className="col-md-6 col-lg-5">
                            <div className="switchable__text">
                                <h2>Dynamic data in the cloud</h2>
                                <p className="lead">
                                   Unlike other Location Intelligence platforms, you can create views and new tables directly in the platform without needing to download the data and then re-upload - saving you valuable time and resources.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </div>
  );
}

AboutHeader.propTypes = propTypes;
AboutHeader.defaultProps = defaultProps;

export default AboutHeader;