import React from 'react';
// import sections
import FlugComponeent from '../components/sections/FlugComponeent';
import FeaturesTiles from '../components/sections/FlugFeatures';
import WhyFlug from '../components/sections/WhyFlug';

import Cta from '../components/sections/Cta';

const Flugx = () => {

  return (
    <>
    <FlugComponeent />
    <FeaturesTiles />
    <WhyFlug />
    <Cta split />
    </>
  );
}

export default Flugx;