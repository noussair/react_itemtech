import React from 'react';

import AboutHeader from '../components/sections/AboutHeader';
import FeaturesTiles from '../components/sections/FlugFeatures';
import FeaturesSplit from '../components/sections/FeaturesSplit';
import Testimonial from '../components/sections/Testimonial';
import Cta from '../components/sections/Cta';

const About = () => {

  return (
    <>
    <AboutHeader />
    
      <Cta split />
    </>
  );
}

export default About;